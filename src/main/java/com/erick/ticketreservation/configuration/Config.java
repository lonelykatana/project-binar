package com.erick.ticketreservation.configuration;

import com.erick.ticketreservation.enumeration.ERole;
import com.erick.ticketreservation.model.users.Roles;
import com.erick.ticketreservation.repository.RolesRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AllArgsConstructor
public class Config {

    private static final Logger LOG = LoggerFactory.getLogger(Config.class);

    private RolesRepository rolesRepository;

    @Bean
    public void prerun() {
        for (ERole c : ERole.values()) {
            try {
                Roles roles = rolesRepository.findByName(c)
                        .orElseThrow(() -> new RuntimeException("Roles not found"));
                LOG.info("Role {} has been found!", roles.getName());
            } catch (RuntimeException rte) {
                LOG.info("Role {} is not found, inserting to DB . . .", c.name());
                Roles roles = new Roles();
                roles.setName(c);
                rolesRepository.save(roles);
            }
        }
    }
}
