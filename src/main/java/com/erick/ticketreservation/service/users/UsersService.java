package com.erick.ticketreservation.service.users;

import com.erick.ticketreservation.model.users.Users;

public interface UsersService {

    void saveUsers(String username, String email, String password);

    void deleteUsersByUsername(String username);

    Users findUsersByUsername(String username);
}
