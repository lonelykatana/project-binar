package com.erick.ticketreservation.service.users;

import com.erick.ticketreservation.model.users.Users;
import com.erick.ticketreservation.repository.UsersRepository;
import lombok.AllArgsConstructor;

import org.springframework.stereotype.Service;

//sudah otomatis dibuat constructornya, dan kemudian dilakukan constructor injection secara otomatis oleh spring
@AllArgsConstructor
@Service
public class UsersServiceImpl implements UsersService {

    private UsersRepository usersRepository;

    @Override
    public void saveUsers(String username, String email, String password) {
        Users users = new Users();
        users.setUsername(username);
        users.setEmail(email);
        users.setPassword(password);
        usersRepository.save(users);
    }

    @Override
    public void deleteUsersByUsername(String username) {
        usersRepository.deleteUsersByUsername(username);
    }

    @Override
    public Users findUsersByUsername(String username) {
        return usersRepository.findUsersByUsername(username);
    }
}
