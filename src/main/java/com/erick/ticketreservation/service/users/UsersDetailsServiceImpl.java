package com.erick.ticketreservation.service.users;

import com.erick.ticketreservation.model.users.Users;
import com.erick.ticketreservation.model.users.UsersDetailsImpl;
import com.erick.ticketreservation.repository.UsersRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class UsersDetailsServiceImpl implements UserDetailsService {

    private UsersRepository usersRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users users = usersRepository.findUsersByUsername(username);
        return UsersDetailsImpl.build(users);
    }
}
