package com.erick.ticketreservation.service.films;

import com.erick.ticketreservation.model.Films;
import com.erick.ticketreservation.repository.FilmsRepository;
import lombok.AllArgsConstructor;

import org.springframework.stereotype.Service;

import java.util.List;

//sudah otomatis dibuat constructornya, dan kemudian dilakukan constructor injection secara otomatis oleh spring
@AllArgsConstructor
@Service
public class FilmsServiceImpl implements FilmsService {

    private FilmsRepository filmsRepository;


    @Override
    public void saveFilms(String filmName, Boolean isTayang) {
        Films films = new Films();
        films.setFilmName(filmName);
        films.setIsTayang(isTayang);
        filmsRepository.save(films);
    }

    @Override
    public void updateFilmsInfoById(String filmName, Boolean isTayang, Long filmId) {
        filmsRepository.setFilmsInfoById(filmName, isTayang, filmId);
    }

    @Override
    public void deleteFilms(Long filmId) {
        filmsRepository.deleteFilmsbyId(filmId);
    }


    @Override
    public List<Films> findAllFilmsTayang() {
        return filmsRepository.findAllFilmsTayang();
    }

    @Override
    public Films findFilmById(Long filmid) {
        return filmsRepository.findFilmById(filmid);
    }


}
