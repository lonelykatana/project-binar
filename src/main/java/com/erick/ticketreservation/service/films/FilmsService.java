package com.erick.ticketreservation.service.films;

import com.erick.ticketreservation.model.Films;


import java.util.List;

public interface FilmsService {

    void saveFilms(String filmName, Boolean isTayang);

    void updateFilmsInfoById(String filmName, Boolean isTayang, Long filmId);

    void deleteFilms(Long filmId);

    List<Films> findAllFilmsTayang();

    Films findFilmById(Long filmid);


}