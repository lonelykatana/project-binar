package com.erick.ticketreservation.service.schedules;

import com.erick.ticketreservation.model.Schedules;


import java.util.List;

public interface SchedulesService {

    void saveSchedule(String jamMulai, String jamSelesai, String tglTayang, Long filmId);

    List<Schedules> testFindFilmsByJadwal(Long filmId);

    Schedules findSchedulesById(Long schedulesId);
}
