package com.erick.ticketreservation.service.schedules;

import com.erick.ticketreservation.model.Films;
import com.erick.ticketreservation.model.Schedules;
import com.erick.ticketreservation.repository.SchedulesRepository;
import com.erick.ticketreservation.service.films.FilmsService;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;


import java.util.List;


//sudah otomatis dibuat constructornya, dan kemudian dilakukan constructor injection secara otomatis oleh spring
@AllArgsConstructor
@Service
public class SchedulesServiceImpl implements SchedulesService {

    private SchedulesRepository schedulesRepository;

    private FilmsService filmsService;

    @Override
    public void saveSchedule(String jamMulai, String jamSelesai, String tglTayang, Long filmId) {
        Schedules schedules = new Schedules();
        schedules.setJamMulai(LocalTime.parse(jamMulai));
        schedules.setJamSelesai(LocalTime.parse(jamSelesai));
        schedules.setTglTayang(LocalDate.parse(tglTayang));
        Films films = filmsService.findFilmById(filmId);
        schedules.setFilmId(films);
        schedulesRepository.save(schedules);

    }


    @Override
    public List<Schedules> testFindFilmsByJadwal(Long filmId) {
        return schedulesRepository.testFindFilmsByJadwal(filmId);
    }

    @Override
    public Schedules findSchedulesById(Long schedulesId) {
        return schedulesRepository.findSchedulesById(schedulesId);
    }
}
