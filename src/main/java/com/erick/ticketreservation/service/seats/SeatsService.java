package com.erick.ticketreservation.service.seats;

public interface SeatsService {
    void addSeats(Character stdName, Integer nmrKursi, Long scheduleId);
}
