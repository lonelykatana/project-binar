package com.erick.ticketreservation.service.seats;

import com.erick.ticketreservation.model.Schedules;
import com.erick.ticketreservation.model.seats.Seats;
import com.erick.ticketreservation.repository.SeatsRepository;
import com.erick.ticketreservation.service.schedules.SchedulesService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class SeatsServiceImpl implements SeatsService {

    private SeatsRepository seatsRepository;
    private SchedulesService schedulesService;

    @Override
    public void addSeats(Character stdName, Integer nmrKursi, Long scheduleId) {
        Seats seats = new Seats();
        Schedules schedules = schedulesService.findSchedulesById(scheduleId);
        seats.setStdName(stdName);
        seats.setNmrKursi(nmrKursi);
        seats.setScheduleId(schedules);
        seatsRepository.save(seats);
    }
}
