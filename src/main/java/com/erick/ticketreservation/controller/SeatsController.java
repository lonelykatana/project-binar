package com.erick.ticketreservation.controller;

import com.erick.ticketreservation.service.seats.SeatsService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//sudah otomatis dibuat constructornya, dan kemudian dilakukan constructor injection secara otomatis oleh spring menggunakan autowired

@Tag(name = "Seats Controller", description = "API for processing Seats entity")
@AllArgsConstructor
@RestController
@RequestMapping("/seats")
public class SeatsController {
    private SeatsService seatsService;

    public void addSeats(Character stdName, Integer nmrKursi, Long scheduleId) {
        seatsService.addSeats(stdName, nmrKursi, scheduleId);
    }
}
