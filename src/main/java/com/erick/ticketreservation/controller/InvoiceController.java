package com.erick.ticketreservation.controller;

import com.erick.ticketreservation.service.invoice.InvoiceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import net.sf.jasperreports.engine.JRException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.io.IOException;

@Tag(name = "Invoice Controller", description = "API for processing Invoice entity")
@AllArgsConstructor
@RestController
@RequestMapping("/cinema/invoice")
public class InvoiceController {

    private InvoiceService invoiceService;

    // tiketnya
    @Operation(summary = "get invoice, which contains, data from users, seats, films, and schedules")
    @GetMapping("/customer/get-invoice")
    public ResponseEntity<HttpStatus> generateInvoice(@RequestParam String username, @RequestParam Character stdName, @RequestParam Integer nmrKursi, @RequestParam Long scheduleId)
            throws IOException, JRException {
        invoiceService.getInvoice(username, stdName, nmrKursi, scheduleId);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
