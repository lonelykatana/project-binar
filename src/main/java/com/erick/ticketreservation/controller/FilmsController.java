package com.erick.ticketreservation.controller;

import com.erick.ticketreservation.model.Films;
import com.erick.ticketreservation.model.Schedules;
import com.erick.ticketreservation.service.films.FilmsService;
import com.erick.ticketreservation.service.schedules.SchedulesService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

//sudah otomatis dibuat constructornya, dan kemudian dilakukan constructor injection secara otomatis oleh spring menggunakan autowired

@Tag(name = "Films Controller", description = "API for processing Films entity")
@AllArgsConstructor
@RestController
@RequestMapping("/cinema/films")
public class FilmsController {
    private FilmsService filmsService;
    private SchedulesService schedulesService;

    private static final Logger LOG = LoggerFactory.getLogger(FilmsController.class);

    //menambah film
    @Operation(summary = "add film in films table")
    @PostMapping("/admin/add-films")
    public ResponseEntity<HttpStatus> addFilms(@Schema(example = "{" +
            "\"filmName\":\"The Tragedy of Macbeth\"," +
            "\"isTayang\":\"true\"," + "}") @RequestBody Map<String, Object> film) {
        filmsService.saveFilms((String) film.get("filmName"), (Boolean) film.get("isTayang"));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    //mengupdate data di dalam table film
    @Operation(summary = "update a row in films table using filmId")
    @PutMapping("/admin/update-films")
    public ResponseEntity<HttpStatus> updateFilmsInfoById(@RequestParam String filmName, @RequestParam Boolean isTayang, @RequestParam Long filmId) {
        filmsService.updateFilmsInfoById(filmName, isTayang, filmId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //menghapus row dari table films berdasarkan film_id
    @Operation(summary = "delete film in films table using filmId")
    @DeleteMapping("/admin/delete-films")
    public ResponseEntity<HttpStatus> deleteFilms(@RequestParam Long filmId) {
        filmsService.deleteFilms(filmId);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    //menampilkan semua film yang sedang tayang dari table films
    @Operation(summary = "show all the films that's showing")
    @GetMapping("/public/show-film-tayang")
    public ResponseEntity<List<Films>> findAllFilmsTayang() {
        List<Films> films1 = filmsService.findAllFilmsTayang();
        LOG.info("Films Shown");
        return new ResponseEntity<>(films1, HttpStatus.OK);
    }

    //menampilkan jadwal dari films yang sedang tayang
    @Operation(summary = "show the schedule of a specific film that's showing")
    @PostMapping("/public/films-by-jadwal")
    public ResponseEntity<List<Schedules>> testFindFilmsByJadwal(@RequestParam Long filmId) {
        List<Schedules> schedules1 = schedulesService.testFindFilmsByJadwal(filmId);
        return ResponseEntity.ok(schedules1);
    }

    //menampilkan film dari table film berdasarkan film_id
    @Operation(summary = "find a spesific film using filmId")
    @PostMapping("/public/films-by-id")
    public ResponseEntity<Films> findFilmsById(@RequestParam Long filmId) {
        Films films = filmsService.findFilmById(filmId);
        return new ResponseEntity<>(films, HttpStatus.OK);
    }
}
