package com.erick.ticketreservation.controller;

import com.erick.ticketreservation.model.Schedules;
import com.erick.ticketreservation.service.schedules.SchedulesService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.Map;

//sudah otomatis dibuat constructornya, dan kemudian dilakukan constructor injection secara otomatis oleh spring menggunakan autowired

@Tag(name = "Schedules Controller", description = "API for processing Schedules entity")
@AllArgsConstructor
@RestController
@RequestMapping("/cinema/schedules")
public class SchedulesController {
    private SchedulesService schedulesService;

    //menambahkan data ke dalam table schedule
    @PostMapping("/admin/add-schedule")
    public String addSchedule(@RequestBody Map<String, Object> schedule) {
        schedulesService.saveSchedule((String) schedule.get("jamMulai"), (String) schedule.get("jamSelesai"), (String) schedule.get("tglTayang"), (Long) schedule.get("filmId"));
        return "Add Films Success!";
    }


    @GetMapping("/public/get-schedule")
    public ResponseEntity<Schedules> findSchedulesById(@RequestParam Long scheduleId) {
        Schedules schedules = schedulesService.findSchedulesById(scheduleId);
        return new ResponseEntity<>(schedules, HttpStatus.OK);
    }

}
