package com.erick.ticketreservation.controller;

import com.erick.ticketreservation.configuration.JwtUtils;
import com.erick.ticketreservation.enumeration.ERole;
import com.erick.ticketreservation.model.responses.JwtResponse;
import com.erick.ticketreservation.model.responses.MessageResponse;
import com.erick.ticketreservation.model.SignupRequest;
import com.erick.ticketreservation.model.users.Roles;
import com.erick.ticketreservation.model.users.Users;
import com.erick.ticketreservation.model.users.UsersDetailsImpl;
import com.erick.ticketreservation.repository.RolesRepository;
import com.erick.ticketreservation.repository.UsersRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/cinema/auth")
@AllArgsConstructor
public class AuthController {

    private AuthenticationManager authenticationManager;
    private UsersRepository usersRepository;
    private RolesRepository rolesRepository;
    private PasswordEncoder passwordEncoder;
    private JwtUtils jwtUtils;

    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    @PostMapping("/signin")
    public ResponseEntity<JwtResponse> authenticateUsers(@Valid @RequestBody Map<String, Object> login) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(login.get("username"), login.get("password"))
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UsersDetailsImpl usersDetails = (UsersDetailsImpl) authentication.getPrincipal();
        List<String> roles = usersDetails.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        return ResponseEntity.ok(new JwtResponse
                (jwt, usersDetails.getUsername(), usersDetails.getEmail(), roles));
    }

    @PostMapping("/signup")
    public ResponseEntity<MessageResponse> registerUsers(@Valid @RequestBody SignupRequest signupRequest) {
        if (Boolean.TRUE.equals(usersRepository.existsByUsername(signupRequest.getUsername()))) {
            return ResponseEntity.badRequest()
                    .body(new MessageResponse("Error : Username is already taken"));
        }

        if (Boolean.TRUE.equals(usersRepository.existsByEmail(signupRequest.getEmail()))) {
            return ResponseEntity.badRequest()
                    .body(new MessageResponse(("Error : Email is already taken")));
        }

        Users users = new Users(signupRequest.getUsername(), signupRequest.getEmail()
                , passwordEncoder.encode(signupRequest.getPassword()));

        Set<String> strRoles = signupRequest.getRoles();
        Set<Roles> roles = new HashSet<>();

        if (strRoles == null) {
            Roles role = rolesRepository.findByName(ERole.CUSTOMER)
                    .orElseThrow(() -> new RuntimeException("Error : roles is not found"));
            roles.add(role);
        } else {
            strRoles.forEach(role -> {
                Roles roles1 = rolesRepository.findByName(ERole.valueOf(role))
                        .orElseThrow(() -> new RuntimeException("Error : Role " + role + " is not found"));
                roles.add(roles1);
            });
        }
        users.setRoles(roles);
        usersRepository.save(users);
        return ResponseEntity.ok(new MessageResponse("User registered successfully"));
    }
}
