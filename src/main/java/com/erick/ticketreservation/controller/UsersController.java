package com.erick.ticketreservation.controller;


import com.erick.ticketreservation.model.users.Users;
import com.erick.ticketreservation.service.users.UsersService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Tag(name = "Users Controller", description = "API for processing Users entity")
//sudah otomatis dibuat constructornya, dan kemudian dilakukan constructor injection secara otomatis oleh spring menggunakan autowired
@AllArgsConstructor
@RestController
@RequestMapping("/cinema/users")
public class UsersController {

    private UsersService usersService;

    private static final Logger LOG = LoggerFactory.getLogger(UsersController.class);

    //TIDAK DIPERLUKAN LAGI KARENA ADD USER SUDAH MENGGUNAKAN SIGNIN


    // ..bisa juga sebagai update, jika username yang dimasukkan sudah ada sebelumnya
    @Operation(summary = "Update a row in users table, only works if the username has already exist in the table")
    @PutMapping("/customer/update-users")
    public ResponseEntity<HttpStatus> updateUsers(@RequestBody Map<String, Object> user) {
        usersService.saveUsers(user.get("username").toString(), user.get("email").toString(),
                user.get("password").toString());
        LOG.info("User updated: {}", user.get("username"));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    //menghapus row dari table users berdasarkan username
    @Operation(summary = "delete row in users table using where username exist")
    @DeleteMapping("/customer/delete-users")
    public ResponseEntity<HttpStatus> deleteUsersByUsername(@RequestParam String username) {
        usersService.deleteUsersByUsername(username);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @GetMapping("/admin/find-users")
    public ResponseEntity<Users> findUsersByUsername(@RequestParam String username) {
        Users users = usersService.findUsersByUsername(username);
        LOG.info("User found : {}", users.getUsername());
        return new ResponseEntity<>(users, HttpStatus.OK);
    }
}
