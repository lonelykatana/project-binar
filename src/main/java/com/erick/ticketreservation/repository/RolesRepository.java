package com.erick.ticketreservation.repository;

import com.erick.ticketreservation.enumeration.ERole;
import com.erick.ticketreservation.model.users.Roles;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RolesRepository extends JpaRepository<Roles, Integer> {
    Optional<Roles> findByName(ERole name);
}
