package com.erick.ticketreservation.repository;

import com.erick.ticketreservation.model.seats.Seats;
import com.erick.ticketreservation.model.seats.SeatsId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SeatsRepository extends JpaRepository<Seats, SeatsId> {

}
