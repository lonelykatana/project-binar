package com.erick.ticketreservation.repository;

import com.erick.ticketreservation.model.Films;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface FilmsRepository extends JpaRepository<Films, Long> {

    @Modifying
    @Query(value = "update films set film_name=?1, is_tayang=?2 where film_id=?3",
            nativeQuery = true)
    void setFilmsInfoById(String filmName, Boolean isTayang, Long filmId);

    @Modifying
    @Query(value = "delete from films where film_id=?1", nativeQuery = true)
    void deleteFilmsbyId(Long filmId);


    @Query(value = "select * from films where is_tayang=true", nativeQuery = true)
    List<Films> findAllFilmsTayang();

    @Query(value = "select * from films where film_id=?1", nativeQuery = true)
    Films findFilmById(Long filmId);


}
