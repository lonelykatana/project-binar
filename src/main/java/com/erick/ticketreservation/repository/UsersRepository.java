package com.erick.ticketreservation.repository;

import com.erick.ticketreservation.model.users.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface UsersRepository extends JpaRepository<Users, String> {


    void deleteUsersByUsername(String username);

    @Query(value = "select * from users where username=?1", nativeQuery = true)
    Users findUsersByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}

