package com.erick.ticketreservation.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;


import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;


@Entity
@Getter
@Setter
@EqualsAndHashCode
@Table(name = "schedules")
public class Schedules implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long scheduleId;


    @Column(name = "tgl_tayang", columnDefinition = "DATE")
    private LocalDate tglTayang;


    @Column(name = "jam_mulai", columnDefinition = "TIME")
    private LocalTime jamMulai;


    @Column(name = "jam_selesai", columnDefinition = "TIME")
    private LocalTime jamSelesai;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "film_id")
    private Films filmId;

    @Override
    public String toString() {
        return "Schedules{" +
                "scheduleId=" + scheduleId +
                ", tglTayang=" + tglTayang +
                ", jamMulai=" + jamMulai +
                ", jamSelesai=" + jamSelesai +
                ", films=" + filmId +
                '}';
    }
}
