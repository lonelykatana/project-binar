package com.erick.ticketreservation.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Getter
@Setter
@EqualsAndHashCode
@Table(name = "films")
public class Films implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long filmId;

    @Column(name = "film_name")
    private String filmName;

    @Column(name = "is_tayang")
    private Boolean isTayang;

    @Override
    public String toString() {
        return "films [  film_id : " + filmId + "" +
                "\n\t\t film_name : " + filmName + " \n\t\t is_tayang : " + isTayang + " ]";
    }

}
