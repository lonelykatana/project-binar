package com.erick.ticketreservation.model.seats;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;

import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor

public class SeatsId implements Serializable {

    @Column(length = 1)
    private Character stdName;

    @Column(length = 3)
    private Integer nmrKursi;


}
