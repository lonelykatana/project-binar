package com.erick.ticketreservation.model.seats;

import com.erick.ticketreservation.model.Schedules;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Getter
@Setter
@Entity
@IdClass(SeatsId.class)
@Table(name="seats")

public class Seats {

    @Id
    private Character stdName;

    @Id
    private Integer nmrKursi;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "schedule_id")
    private Schedules scheduleId;

}
