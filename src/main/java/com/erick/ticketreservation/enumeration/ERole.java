package com.erick.ticketreservation.enumeration;

public enum ERole {
    ADMIN, CUSTOMER
}
