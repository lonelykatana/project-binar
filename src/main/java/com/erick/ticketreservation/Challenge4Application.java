package com.erick.ticketreservation;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

import java.util.logging.Level;
import java.util.logging.Logger;

@SpringBootApplication
@ConfigurationPropertiesScan
public class Challenge4Application {

    public static void main(String[] args) {
        Logger logger = Logger.getLogger(Challenge4Application.class.getName());
        SpringApplication.run(Challenge4Application.class, args);
        logger.log(Level.INFO, "Hello World!");



    }

}
