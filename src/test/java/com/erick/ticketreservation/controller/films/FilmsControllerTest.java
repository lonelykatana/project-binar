package com.erick.ticketreservation.controller.films;

import com.erick.ticketreservation.controller.FilmsController;
import com.erick.ticketreservation.model.Films;
import com.erick.ticketreservation.model.Schedules;
import com.erick.ticketreservation.repository.FilmsRepository;
import com.erick.ticketreservation.service.films.FilmsService;
import com.erick.ticketreservation.service.schedules.SchedulesService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.logging.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class FilmsControllerTest {

    @Autowired
    private FilmsController filmsController;

    @Autowired
    private FilmsService filmsService;

    @Autowired
    private SchedulesService schedulesService;

    @Test
    @DisplayName("0. Tambah Film Baru")
    void addFilms() {
        Map<String, Object> film = new HashMap<>();
        film.put("filmName", "The Northmen");
        film.put("isTayang", true);
        Assertions.assertEquals(new ResponseEntity<>(HttpStatus.CREATED), filmsController.addFilms(film));
    }

    @Test
    @DisplayName("1. Tambah Film Baru")
    void updateFilms() {
        String filmName = "The Northmen";
        Boolean isTayang = false;
        Long filmId = 2L;
        Assertions.assertEquals(new ResponseEntity<>(HttpStatus.OK),
                filmsController.updateFilmsInfoById(filmName, isTayang, filmId));
    }

    @Test
    @DisplayName("2. Hapus Film")
    void deleteFilms() {
        Long filmId = 2L;
        Assertions.assertEquals(new ResponseEntity<>(HttpStatus.ACCEPTED), filmsController.deleteFilms(filmId));
    }

    @Test
    @DisplayName("3. Tampilkan Film - Film yang Sedang Tayang")
    void showFilms() {
        List<Films> films1 = filmsService.findAllFilmsTayang();
        Assertions.assertEquals(new ResponseEntity<>(films1, HttpStatus.OK), filmsController.findAllFilmsTayang());
    }

    @Test
    @DisplayName("4. Tampilkan Jadwal Film yang Sedang Tayang Menggunakan filmId")
    void filmsSchedule() {
        Long filmId = 2L;
        List<Schedules> schedules1 = schedulesService.testFindFilmsByJadwal(filmId);
        Assertions.assertEquals(new ResponseEntity<>(schedules1, HttpStatus.OK), filmsController.testFindFilmsByJadwal(filmId));
    }

    @Test
    @DisplayName("5. Cari Film Menggunakan filmId")
    void findFilms() {
        Long filmId = 2L;
        Films films = filmsService.findFilmById(filmId);
        Assertions.assertEquals(new ResponseEntity<>(films, HttpStatus.OK), filmsController.findFilmsById(filmId));
    }
}
