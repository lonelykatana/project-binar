package com.erick.ticketreservation.controller.users;

import com.erick.ticketreservation.controller.UsersController;
import com.erick.ticketreservation.model.users.Users;
import com.erick.ticketreservation.service.users.UsersService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

@SpringBootTest
class UsersControllerTest {

    @Autowired
    private UsersController usersController;

    @Autowired
    private UsersService usersService;

    @Test
    @DisplayName("6. Update User")
    void updateUsers() {
        Map<String, Object> user = new HashMap<>();
        user.put("username", "Erick234");
        user.put("email", "erick234@gmail.com");
        user.put("password", "erick234");
        Assertions.assertEquals(new ResponseEntity<>(HttpStatus.OK), usersController.updateUsers(user));
    }

    @Test
    @DisplayName("7. Hapus User Menggunakan Username")
    void deleteUsers() {
        String username = "Erick234";
        Assertions.assertEquals(new ResponseEntity<>(HttpStatus.ACCEPTED), usersController.deleteUsersByUsername(username));
    }

    @Test
    @DisplayName("8. Cari User Menggunakan username")
    void findUsers() {
        String username = "Erick234";
        Users users = usersService.findUsersByUsername(username);
        Assertions.assertEquals(new ResponseEntity<>(users, HttpStatus.OK), usersController.findUsersByUsername(username));
    }
}


