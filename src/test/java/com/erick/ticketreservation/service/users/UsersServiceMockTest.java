package com.erick.ticketreservation.service.users;

import com.erick.ticketreservation.repository.UsersRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UsersServiceMockTest {

    @Mock
    private UsersRepository usersRepository;

    private UsersService usersService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        this.usersService = new UsersServiceImpl(usersRepository);
    }

    @Test
    void saveUsers() {
        String username = "Erick234";
        String email = "Erick234@gmail.com";
        String password = "erick234";
        Assertions.assertDoesNotThrow(() -> usersService.saveUsers(username, email, password));
    }

    @Test
    void deleteUsers() {
        String username = "Erick234";
        Assertions.assertDoesNotThrow(() -> usersService.deleteUsersByUsername(username));
    }

    @Test
    void findUsers() {
        String username = "Erick234";
        Assertions.assertEquals(usersRepository.findUsersByUsername(username), usersService.findUsersByUsername(username));
    }
}
