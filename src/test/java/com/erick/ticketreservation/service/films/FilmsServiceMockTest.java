package com.erick.ticketreservation.service.films;

import com.erick.ticketreservation.repository.FilmsRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class FilmsServiceMockTest {

    @Mock
    private FilmsRepository filmsRepository;

    private FilmsService filmsService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        this.filmsService = new FilmsServiceImpl(filmsRepository);
    }

    @Test
    void saveFilms() {
        String filmName = "Gangs of New York";
        Boolean isTayang = true;
        Assertions.assertDoesNotThrow(() -> filmsService.saveFilms(filmName, isTayang));
    }

    @Test
    void updateFilms() {
        String filmName = "Gangs of New York";
        Boolean isTayang = true;
        Long filmId = 2L;
        Assertions.assertDoesNotThrow(() -> filmsService.updateFilmsInfoById(filmName, isTayang, filmId));
    }

    @Test
    void deleteFilms() {
        Long filmId = 2L;
        Assertions.assertDoesNotThrow(() -> filmsService.deleteFilms(filmId));
    }

    @Test
    void showFilmsTayang() {
        Assertions.assertEquals(filmsRepository.findAllFilmsTayang(), filmsService.findAllFilmsTayang());
    }

    @Test
    void findFilm() {
        Long filmId = 2L;
        Assertions.assertEquals(filmsRepository.findFilmById(filmId), filmsService.findFilmById(filmId));
    }
}
